:imagesdir: ./images

==== Create a Sales Order

A sales order is created when a customer wants to purchase new products or services.

To create a sales order:

. Retrieve the customer or contact using the _Global Search_ bar.
. Click the *View* icon.
. Validate the customer or contact.
. Click _Offers_.
. Select a top-level offer from either _Promotion Offers_ or _Recommended Offers_.
    
    NOTE: The offers that are greyed out cannot be selected.

. Select the required child offers (and features) associated with the Offer Hierarchy group.
    
    NOTE: To clear the selections, click Clear Selection link. 

. After the selection, click *Proceed to Configuration* button.
. Configure the service according to requirements.

    NOTE: 
    * Configuring SIM Options is a mandatory step.
    * Multiple services can be added using *Add another service* button.
    * Add-ons can be added to the service based on availability. 

. Click *Add to Cart*.
    
    NOTE: Button gets enabled only after successful configuration.

. Verify the cart details.
. Click the *Next* button.

    NOTE: If there are any discrepancies, click Return to Configuration link.

. Verify the _Installation_ details.
. Click the *Billing* button.

    NOTE: 
    * Details like Location, Installation Date, and the Engineers who perform the installation are displayed in this section.
    * Click the edit icon to add installation notes and contact number. 
    * In case of no assistance required to install a service, use the toggle button to mark it as Self Installation.
    * Click *Confirm* button to save the changes.

. Add the _Billing_ details.
. Click the *Payment* button.
    
    * Use *Activate Auto Pay* toggle button to activate regular monthly payment for the customer and to send an invoice each month.
    * If the *Activate Auto Pay* toggle button is enabled, enter the customer's credit card details.
    * Select either _Paper_ or _Electronic_ invoice type. 
    * If the Paper invoice type is selected, the invoice can be sent to a different address using _Send to a different address_ link.
    * Click the edit icon to add a different address.
    * Click the *Confirm* button to save the changes.
    * Verify the billing address.

        NOTE: 
        * The default address will be same as the delivery address. 
        * Click the edit icon to make changes to the address.
        * Click the *Confirm* button to save the changes.
    
. Add the _Payment_ details. 
. Click the *Confirm* button.

    * Amount to be paid for the offers selected is displayed in this section.
    * Additional information like VAT etc. can be entered in the _Additional Information_ section.
    * Select the mode of payment (Credit Card, Loyal Points, and Cash).
    * A payment mode combination is allowed. For example, if the amount to be paid is $100, the customer can pay $90 through credit card and the remaining $10 via cash.

. Review the details and confirm the payment by clicking *Complete* button. 