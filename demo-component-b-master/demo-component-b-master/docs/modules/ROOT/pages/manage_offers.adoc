:imagesdir: ./images

=== Manage Offers

A product offer is a collection of products that are offered together for sale to a customer. Most product offers include child product offers.

When selling or altering a product, offers can be added or removed from the hierarchy tree using the associated check box.

* Offers can be configured to:

*** Allow multiple product offers of the same type to be sold.
*** Be automatically selected when a product offer is sold.

* When displayed in a Product Enquiry, Sales Order, or Alter Product Options, the Offer Hierarchy tree displays:
**** Offers that allow multiple product offers with an indicator showing the minimum and maximum number of times the product offer can be sold. For example, Basic Family and Friends - max 4

* Default optional product offers with (D) after the text. For example:
**** Call Waiting (D)
**** Prepaid APN - max 6 (D)
**** Service-specific product offers with (S) after the text. For example, Residential Phone (S)
**** Offers that have entitlements with (E) after the text. For example, 20 Gbyte Data (E)

This section describes how to manage offers.