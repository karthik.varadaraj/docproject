:imagesdir: ./images

===== Add or Remove a Feature

A feature is added to or removed from a product when a customer requests (or purchases) an optional extra associated with their active product, or decides that they no longer need an existing feature.

Some features are sold as standard with a product, and cannot be added or removed.

To add rich product feature:

. Retrieve the customer using the _Global Search_ bar.
. Click the _View_ icon.
. Validate the customer or contact.
. Choose the associated contact.
. Click the *Validate Contact* button.
. Validate the contact details using _Finger ID Validation_ or _Security Questions_. 

    NOTE: To skip the validation, use the Skip Validation link.

. Select a product listed under the _Purchased Products_ tab.
. Click on the active plan card.
. Click the hamburger menu.
. Select the _Add Child Offer_ option.
. Select the associated optional child offer.
. Click the _Proceed to Configuration_ button.
. Submit the sales order.

NOTE: For more information on submitting sales orders, click Sales Order.