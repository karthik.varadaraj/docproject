:imagesdir: ./images

==== Cancel an Action

Action is canceled when it is no longer required. 

    NOTE: A case cannot be closed until all the actions associated are completed or canceled.

To cancel an action:

* Get the next action, or retrieve an action.