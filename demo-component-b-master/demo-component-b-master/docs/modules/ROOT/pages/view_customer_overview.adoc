:imagesdir: ./images

==== Customer Overview

The customer overview tab is a quick summary of all the details associated with the customer. It consists of details like:

* Products & Services - products and offers subscription details are displayed in this section.
* Last Transactions - list of latest transactions made by the customer is displayed in this section.
* Billing - billing details of the customer are displayed in this section.
* Last Orders - latest orders made by the customer are displayed in this section.
* Alerts - customer alerts are displayed in this section. 
* Last Cases - a list of cases related to the customer is displayed in this section.
* Offers - associated offers with the customer are displayed in this section.

NOTE: Click _See All_ link on the widget to open the details in the respective tab.