:imagesdir: ./images

==== Retrieve a Sales Order

A sales order is retrieved when it needs to be updated, submitted, or canceled.

To retrieve a sales order:

. Retrieve the customer or contact using the _Global Search_ bar.
. Select the customer or contact.
. Click the *View* icon.
. Select the associated contact.
. Click the *Validate Contact* button.
. Validate the contact details using _Finger ID Validation_ or _Security Questions_. 

    NOTE: To skip the validation, use the Skip Validation link.

. Select the _Orders_ tab.
. Select a sales order.
. Click the _Expand_ icon to view the order details.