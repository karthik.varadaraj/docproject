:imagesdir: ./images

=== Contact

A contact is the person who represents an individual or organisation and contacts the call centre. 
Contacts can be:

    * Categorised by contact types (for example, residential and business)
    * Associated with one or more customers (subscribers)
    * Not associated with customers (prospects).

Each case must have an associated contact, unless the case is a query from a person who wishes to remain anonymous.