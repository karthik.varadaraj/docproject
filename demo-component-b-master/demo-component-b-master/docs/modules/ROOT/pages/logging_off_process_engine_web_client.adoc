:imagesdir: ./images

=== Logging Off Web Client

To log off Customer Management, click the *LOG OUT* link on the CSR Profile.

If there are any unsaved changes, a *LOG OUT* pop-up prompts the user to view the changes, click *Save & Logout* to save the changes or discard the changes by clicking *Log Out without saving* button, or cancel logging out by clicking *Cancel* button.

If the changes are saved, a *LOG OUT* pop-up prompts the user to confirm the logout. Click the *Confirm* button to log out. 