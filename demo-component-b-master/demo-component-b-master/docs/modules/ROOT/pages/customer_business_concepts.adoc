:imagesdir: ./images

=== Customer

A customer buys products, services, and features. 

A customer can be an organisation or a person, but a contact must be a person. A customer must be represented by one or more associated contacts.

Customers are categorised by customer types (for example, residential and business). 

Customers can be associated with each other in a parent-child relationship. For example, an organisation's head office can be a parent customer with branch offices as child customers.