:imagesdir: ./images

==== Update a Sales Order

A sales order can be updated after it is created, and before it is submitted or canceled if the customer wants to add or remove products, services, or features.

To update a sales order:

. Retrieve the customer or contact using the _Global Search_ bar.
. Select the customer or contact and click the *View* icon.
. Select the _Orders_ tab.
. Select a sales order with pending, or validated status.

    NOTE: Sales orders that have accepted or canceled status cannot be updated.

. Click the _Expand_ icon to view the order details.
. Click the _Edit_ icon to update the sales order. 
. Update the sales order details.
. Click the *Proceed to Cart* button.