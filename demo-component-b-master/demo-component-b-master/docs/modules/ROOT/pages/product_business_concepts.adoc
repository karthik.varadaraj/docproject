:imagesdir: ./images

=== Product

A product is a combination of services, inventory, and associated features designed to meet customer requirements. 

A customer can have many purchased products with associated services, inventory, product offers, and features.