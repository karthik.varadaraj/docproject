:imagesdir: ./images

=== Manage Action

An action is an event, procedure, or task occurring at a defined time or over a defined period. Actions are used for managing workflows and campaigns, and provide a history of events associated with a customer. For example, activities relating to a case such as following up with a customer or investigating complaints are considered to be actions.

Action groups consist of manageable sets of related activities involved in complex business processes.

Actions can be assigned to logical work groups instead of individuals. Group managers can assign activities from the group’s queue to individuals. Alternately, the Get Next Action operation can be used by individuals to assign the next unassigned action to themselves.

This section describes how to manage action.