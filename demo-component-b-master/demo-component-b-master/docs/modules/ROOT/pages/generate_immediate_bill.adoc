:imagesdir: ./images

==== Generate Immediate Bill

An immediate bill is generated to add charges to an existing invoice for using an additional product or service by the customers. These charges are created outside the standard billing cycle and presented to the customer in future bill run.

To generate an immediate bill:

. Retrieve the customer using the _Global Search_ bar.
. Click the *View* icon.
. Validate the customer or contact.
. Click the _Customer Overview_ tab on the secondary navigation menu.
. Click the _See All_ link on the _Billing_ widget. 
. Click the dropdown next to _ACCOUNT ACTIONS_.
. Select the _GENERATE IMMEDIATE BILL_ option.
. Select the _Start Date_.
. Click the *Generate* button.