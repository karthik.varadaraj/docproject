:imagesdir: ./images

==== Cancel a Payment

A payment is canceled when:

* It is rejected.
    ** Payments are applied immediately, but can subsequently be rejected on notification from the financial institution responsible for processing the payment. For example, payments can be rejected when a cheque fails to clear, there are insufficient funds for a direct debit, or a credit card is declined.

* Its details are incorrect.
    ** A payment contains incorrect payment details (for example, a payment amount).

To cancel a payment:

. Retrieve the customer using the _Global Search_ bar.
. Click the *View* icon.
. Validate the customer or contact.
. Click the _Customer Overview_ tab on the secondary navigation menu.
. Click the _See All_ link on the _Billing_ widget. 
. Click the _Payments_ tab.
. Select payment by applying the filter.
. Click the _Hamburger_ icon.
. Select the _Cancel Payment_ option.