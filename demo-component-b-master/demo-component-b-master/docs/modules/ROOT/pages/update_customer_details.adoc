:imagesdir: ./images

==== Update Customer Details

Customer details need to be updated when the contact informs that there is a change in billing information (for example, a new credit card number or expiry date for direct debit payments).

To update customer details:

. Search the customer using _Global Search_ bar.
. Select the customer from the _Customers Results_. 
. Click the *View* icon to open the customer information.
. Select the associated contact.
. Click the *Validate Contact* button.
. Validate the contact details using _Finger ID Validation_ or _Security Questions_. 

    NOTE: The user can skip the validation by clicking the Skip Validation link.

. Click the _Customer Information_ tab.
. Click the *Edit* button to edit the customer details.

    NOTE: The customer details in the Overview tab can be updated.

. Update the customer details.
. Click the *Save* button.