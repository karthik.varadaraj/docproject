:imagesdir: ./images

===== Adding VAS

A customer can add VAS (Value Added Service) to the existing product.

To add VAS:

. Retrieve the customer using the _Global Search_ bar.
. Click the _View_ icon.
. Validate the customer or contact.
. Choose the associated contact.
. Click the *Validate Contact* button.
. Validate the contact details using _Finger ID Validation_ or _Security Questions_. 

    NOTE: To skip the validation, use the Skip Validation link.

. Select a product listed under the _Purchased Products_ secondary navigation menu.
. Click on the active plan card.
. Click the _VAS_ tab.
. Click the *Add* button.
. Select the _Reason for this change_.
. Click the *Add Child Offer* button.
. Select the offer checkbox depending on the requirements.
. Click *Proceed to Configuration* button. 
. Create a sales order.

    NOTE: For more information on Sales Order, click Create Sales Order.