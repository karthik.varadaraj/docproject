:imagesdir: ./images

==== Add a Secondary Contact for a Customer

A secondary contact is typically associated with a customer when the primary contact is not available. For example, when the person nominated as primary contact for an organization is on vacation, the person nominated as the secondary contact becomes the communication point for all customer accounts. 

Similarly, a residential customer can nominate a family member as a secondary contact when the primary contact is not available.

To add a secondary contact for a customer:

. Retrieve the customer using the _Global Search_ bar.
. Select the customer from the _Customer Results_.
. Click the _View_ icon.
. Choose the associated contact.
. Click the *Validate Contact* button.
. Validate the contact details using _Finger ID Validation_ or _Security Questions_. 

    NOTE: To skip the validation, use the Skip Validation link.

. Click the _Customer Information_ tab.
. Click the _Associated Contacts_ tab.
. Click the *Add* button

If the _National ID_ is available:

. Enter the _National ID_.
. Click the _Search_ icon.
. Validate the contact details using _Finger ID Validation_ or _Security Questions_. 

    NOTE: To skip the validation, use the Skip Validation link.

. Click the *Complete Validation* button. 
. Review the representative details and click the *Continue* button. 
. Upload the representative letter by clicking on the _cloud_ icon.

    NOTE: Click the Proceed without Representative Letter checkbox if the representative letter is not available.

. Click the *Upload* button.

If the _National ID_ is not available:

. Create a secondary contact by clicking *Blank New Form*.
. Enter the values in the required fields. +
    * Enter the _Personal Information_ of contact. +
    * Enter contact details like _Phone_ and _Email ID_. +
    * Enter the _Address_. +

    NOTE: All the fields are mandatory. 

. Click *Continue*.
. Upload the representative letter by clicking on the _cloud_ icon.

    NOTE: Click the Proceed without Representative Letter checkbox if the representative letter is not available.

. Click the *Upload* button.

