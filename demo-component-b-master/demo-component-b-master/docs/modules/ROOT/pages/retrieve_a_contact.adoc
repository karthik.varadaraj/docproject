:imagesdir: ./images

==== Retrieve a Contact

Contact information is retrieved using the _Global Search_ bar to verify whether the caller is authorised to enquire about a customer's account and services. The contact information can be retrieved using different search criteria like contact _Name_, _National ID_, _MSISDN_, _IMSI_, Email ID, and _Passport ID_.  

To retrieve contact information using the _Global Search_ bar:

. On the _Home_ page, select _Contacts_ from the primary dropdown of the _Global Search_ bar.
. Select the search criteria from the secondary dropdown.
. Enter the details depending on the criteria selected.
. Click the _Search_ icon.